from flask import Flask, render_template

app=Flask(__name__) #variable global

@app.route('/')
def index():
    return render_template('index.html', data="Menu Principal")

@app.route('/empleados')
def empleados():
    return render_template('empleados.html')

@app.route('/clases')
def clases():
    return render_template('clases.html')

@app.route('/horarios')
def horarios():
    return render_template('horarios.html')

if __name__=="__main__":
    app.run(debug=True)